//Vue Instance
new Vue({
    el: '#app', //Object
    data: { //Mewakili data yang ada di ToDoList
        //Memuat data yang ada di list
        items: [],
        item: '', //data untuk menampung nilai yang akan di masukan
    },
    //Methods event Button Click
    methods: {
        addItem(/*parameter*/){
            //dia menangkap nilai dari item melalui "text box"
            //kemudian di masukan ke dalam variabel "items"
            this.items.push(this.item)
            //menyimpan item yang sudah di push kedalam LocalStorage
            localStorage.setItem(/*Key*/ 'items', /*value*/ JSON.stringify(this.items))//Fungsi JSON untuk mengubah ke string
            this.item ='' //Membersihkan input form
        },
        //Function dan Method Remmove Item
        removeItem(/*Parameter*/item){
            //Sebelum menghapus item, harus mengetahui nilai indexnya terlebih dahulu
            const index = this.items.indexOf(item)
            this.items.splice(index/*jumlah index*/, 1 /*Panjang Index*/)//Fungsi untuk menghapus item
            localStorage.setItem(/*Key*/ 'items', /*value*/ JSON.stringify(this.items))//Fungsi JSON untuk mengubah ke string
        }
    },
    //Function created untuk membaca LocalStorage
    created(){
        this.items = JSON.parse(localStorage.getItem(/*key*/'items')) || [] //this.items adalah fungsi untuk menyimpan nilai yang diinputkan
    }
})